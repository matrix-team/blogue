title:   Welcome to Matrix on Debian blog
layout:  post
author:  Andrej Shadura
date:    2019-05-16 15:02

This is the first blog post on this *Matrix on Debian* blog. [The Debian Matrix team](https://tracker.debian.org/teams/debian-matrix/)
will be regularly posting here updates on the progress of the packaging work we do, and the overall status of [Matrix.org](https://matrix.org) software in Debian.

Come chat to us in the [Matrix room](https://matrix.to/#/#debian-matrix:matrix.org) (`#debian-matrix:matrix.org`) we created!
