title:   June 2019 Matrix on Debian update
layout:  post
author:  Andrej Shadura
date:    Fri, 26 Jun 2019 14:36:00 -0500

This is an update on the state of Matrix-related software in Debian.

## Synapse

Unfortunately, the recently published Synapse 1.0 didn’t make it into
Debian Buster, which is due to be released next week, so if you install
0.99.2 from Buster, you need to update to a newer version which will
be available from backports shortly after the release.

Originally, 0.99 was meant to be the last version before 1.0, but due
to a bunch of issues discovered since then, some of them
security-related, new incompatible room format was introduced in 0.99.5.
This means 0.99.2 currently in Debian Buster is going to only
see limited usefulness, since rooms are being upgraded to the new format
as 1.0 is being deployed across the network.

For those of you running forever unstable Sid, good news: Synapse 1.0 is now
available in unstable! ACME support has not yet been enabled, since it
requires a few packages not yet in Debian (they’re currently in the [NEW] queue).
We hope it will be available soon after Buster is released.

## Quaternion

![The main window of Quaternion showing the Unofficial Debian chat room in Matrix, a list of members of the room on the right, and a list of channels on the left](/blogue/images/quaternion.png)

[Quaternion](https://github.com/QMatrixClient/Quaternion) 0.0.9.4 is being packaged by Hubert Chathi, soon to be uploaded.
Hubert has already updated and uploaded [libqmatrixclient](https://ftp-master.debian.org/new/libqmatrixclient_0.5.2-1.html) and [olm](https://ftp-master.debian.org/new/olm_3.1.3+dfsg-1.html), which are waiting in [NEW].

## Circle

![On the left, a terminal window with a Curses front-end of Circle running. On the right, a graphical window running a GTK 2 front-end of Circle. Both window show the same window with IRC connection logs](/blogue/images/circle-two-fes.png)

There’s been some progress on packaging [Circle](http://www.leonerd.org.uk/code/circle/),
a modular IRC client with Matrix support. The backend and IRC support have been available
for some time in Debian already, but to be useful, it also needs a user-interfacing front-end.
The GTK 2 front-end has just been uploaded to Debian, as have the necessary Perl modules for
Matrix support. All of the said packages are now being reviewed in the [NEW] queue.

## Fractal

Early in June, Andrej Shadura looked into packaging [Fractal](https://gitlab.gnome.org/danigm/fractal),
but found a few crates being of incompatible versions in Debian compared to what upstream
expects. A current blocker is a pending release of [rust-phf](https://github.com/sfackler/rust-phf/pull/152).

## Get in touch

Come chat to us in [`#debian-matrix:matrix.org`](https://matrix.to/#/#debian-matrix:matrix.org)!

[NEW]: https://ftp-master.debian.org/new.html
